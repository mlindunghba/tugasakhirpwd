<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//santri
$router->post('/santri', 'santriController@create');
$router->get('/santri', 'santriController@read');
$router->post('/santri/{id}', 'santriController@update');
$router->delete('/santri/{id}', 'santriController@delete');
$router->get('santri/{id}', 'santriController@detail');
//end

//pembimbing
$router->post('/pembimbing', 'pembimbingController@create');
$router->get('/pembimbing', 'pembimbingController@read');
$router->post('/pembimbing/{id}', 'pembimbingController@update');
$router->delete('/pembimbing/{id}', 'pembimbingController@delete');
$router->get('pembimbing/{id}', 'pembimbingController@detail');
//end
