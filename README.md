# Tugas Akhir PWD | Sistem Informasi Madrasah

Nama  : Mochamad Lindung Hasti Budi Aldany

Kelas : XII RPL 2

Absen : 23


Ini adalah tugas akhir pwd saya berupa Aplikasi Sistem Informasi Madrasah , merupakan sebuah aplikasi berbasis web yang dibuat menggunakan lumen (backend) dan
vue.js (frontend). Aplikasi digunakan untuk menyimpan data penting yang menyangkut hal hal tentang Madrasah.
Seperti :

* Data Santri
* Data Pembimbing

Untuk Link Clone nya : 

> https://gitlab.com/mlindunghba/tugasakhirpwd 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Panduan Penggunaan Aplikasi

## 1. Beranda
Homepage/Beranda adalah halaman identitas suatu halaman website.


## 2. Read Data
Halaman yang dapat memberikan data data serta dapat melakukan Tambah Data, Edit Data dan Hapus Data.


## 3. Create Data
Halaman yang dapat membuat data dan datanya langsung masuk ke database.


## 4. Edit Data
Halaman yang dapat Mengubah suatu data serta di database nya pun ikut berubah.


Sekian, Mungkin itu hanya sedikit penjelasan dari saya tentang Aplikasi ini, semoga bermanfaat.
Terimakasih.
