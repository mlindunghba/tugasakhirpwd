<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 */

class pembimbing extends Model
{
	public $table = 'pembimbing';

	protected $fillable = [
		'nama_lengkap',
		'tgl_lahir',
		'alamat',
		'no_telp',
		'jenis_kelamin',
		'mata_pelajaran'
	];
}