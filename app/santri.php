<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 */

class santri extends Model
{
	public $table = 'santri';

	protected $fillable = [
		'nama_lengkap',
		'tgl_lahir',
		'jenis_kelamin',
		'alamat'
	];
}