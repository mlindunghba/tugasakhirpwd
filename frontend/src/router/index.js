import Vue from 'vue'
import Router from 'vue-router'
import santri from '@/components/santri'
import pembimbing from '@/components/pembimbing'
import santriForm from '@/components/santriForm'
import pembimbingForm from '@/components/pembimbingForm'

Vue.use(Router)

export default new Router({
	routes: [
	{
		path: '/santri',
		name: 'santri',
		component: santri
	},	
	{
		path: '/santri/create',
		name: 'santriCreate',
		component: santriForm
	},
	{
		path: '/santri/:id',
		name: 'santriEdit',
		component: santriForm	
	},
	{
		path: '/pembimbing',
		name: 'pembimbing',
		component: pembimbing
	},
	{
		path: '/pembimbing/create',
		name: 'pembimbingCreate',
		component: pembimbingForm
	},
	{	
		path: '/pembimbingform',
		name: 'pembimbingForm',
		component: pembimbingForm
	},
	{
		path: '/pembimbing/:id',
		name: 'pembimbingEdit',
		component: pembimbingForm	
	}
	]
})